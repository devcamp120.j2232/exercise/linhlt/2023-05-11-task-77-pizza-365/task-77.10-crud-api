package com.devcamp.pizza365;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task79FromErdToEntityJpaQueryinActionApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task79FromErdToEntityJpaQueryinActionApplication.class, args);
	}

}
