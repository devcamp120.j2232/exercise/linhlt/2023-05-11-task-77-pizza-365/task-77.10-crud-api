package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.Country;
import com.devcamp.pizza365.repository.ICountryRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CountryController {

	@Autowired
	ICountryRepository countryRepository;
	@GetMapping("/countries")
	public ResponseEntity<List<Country>> getAllCountries(@RequestParam(value = "page", defaultValue = "1") String page,
	@RequestParam(value = "size", defaultValue = "5") String size) {
		try {
			List<Country> countryList = countryRepository.findAll();
			return new ResponseEntity<>(countryList, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/countries/{id}")
	public ResponseEntity<Country> getCountryById(@PathVariable Long id) {
		try {
			Optional<Country> country = countryRepository.findById(id);
			if (country.isPresent()){
				return new ResponseEntity<>(country.get(), HttpStatus.OK);
			}
			else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/countries")
	public ResponseEntity<Object> createCountry(@Valid @RequestBody Country Country) {
		try {
			Country country = new Country();
			country.setCountryName(Country.getCountryName());
			country.setCountryCode(Country.getCountryCode());
			country.setRegions(Country.getRegions());
			Country savedRole = countryRepository.save(country);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Country: "+e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/countries/{id}")
	public ResponseEntity<Object> updateCountry(@PathVariable("id") Long id, @Valid @RequestBody Country Country) {
		try {
			Optional<Country> countryData = countryRepository.findById(id);
			if (countryData.isPresent()) {
				Country newCountry = countryData.get();
				newCountry.setCountryName(Country.getCountryName());
				newCountry.setCountryCode(Country.getCountryCode());
				newCountry.setRegions(Country.getRegions());//khi khai báo country thì kèm theo đối tượng regions
				Country savedCountry = countryRepository.save(newCountry);
				return new ResponseEntity<>(savedCountry, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to update specified Country: "+e.getCause().getCause().getMessage());
		}
	}
	
	@DeleteMapping("/countries/{id}")
	public ResponseEntity<Object> deleteCountryById(@PathVariable Long id) {
		try {
			Optional<Country> optional= countryRepository.findById(id);
			if (optional.isPresent()) {
				countryRepository.deleteById(id);
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}			
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/countries-count")
	public long countCountry() {
		return countryRepository.count();
	}
	
	@GetMapping("/countries/check/{id}")
	public boolean checkCountryById(@PathVariable Long id) {
		return countryRepository.existsById(id);
	}
	
	@GetMapping("/country/code/{code}")
	public Country getCountryByCode(@PathVariable String code) {
		return countryRepository.findByCountryCode(code);
	}
	
	@GetMapping("/country/containing-code/{code}")
	public Country getCountryByContainingCode(@PathVariable String code) {
		return countryRepository.findByCountryCodeContaining(code);
	}

	@GetMapping("/country/{countryName}")
	public ResponseEntity<List<Country>> getAllCountryByCountryNameLike1(@PathVariable String countryName) {
		try {
			List<Country> countries = new ArrayList<Country>();
			countryRepository.findCountryByCountryNameLike(countryName).forEach(countries::add);
			return new ResponseEntity<>(countries, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	//Pageable
	@GetMapping("/countries5")
	public ResponseEntity<List<Country>> getFiveCountries(@RequestParam(value = "page", defaultValue = "1") String page,
	@RequestParam(value = "size", defaultValue = "5") String size) {
		try {
			Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
			List<Country> countryList = new ArrayList<Country>();
			countryRepository.findAll(pageWithFiveElements).forEach(countryList::add);
			return new ResponseEntity<>(countryList, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
