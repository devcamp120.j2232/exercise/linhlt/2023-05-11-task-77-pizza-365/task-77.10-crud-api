package com.devcamp.pizza365.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.*;
import com.devcamp.pizza365.model.ExistsData;
import com.devcamp.pizza365.repository.*;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ProductController {

	@Autowired
	IProductRepository gProductRepository;

	@Autowired
	IProductLineRepository gProductLineRepository;

	@GetMapping("/products")
	public ResponseEntity<List<Product>> getAllProduct() {
		try {
			List<Product> vProducts = new ArrayList<Product>();
			gProductRepository.findAll().forEach(vProducts::add);
			return new ResponseEntity<>(vProducts, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/product/details/{id}")
	public ResponseEntity<Object> getProductById(@PathVariable Integer id) {
		Optional<Product> vProductData = gProductRepository.findById(id);
		if (vProductData.isPresent()) {
			try {
				Product vProduct = vProductData.get();
				return new ResponseEntity<>(vProduct, HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			Product vProductNull = new Product();
			return new ResponseEntity<>(vProductNull, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/product/exists/{productCode}")
	public ResponseEntity<Object> isExistsProductCode(@PathVariable String productCode) {
		try {
			boolean vProductCode = gProductRepository.existsByProductCode(productCode);
			ExistsData vExistsData = new ExistsData();
			if (vProductCode) {
				vExistsData.setName(productCode);
				vExistsData.setExists(true);
			} else {
				vExistsData.setName(productCode);
				vExistsData.setExists(false);
			}
			return new ResponseEntity<>(vExistsData, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/product/create/{productLineId}")
	public ResponseEntity<Object> createProduct(@PathVariable Integer productLineId,
			@Valid @RequestBody Product paramProduct) {
		Optional<ProductLine> vProductLineData = gProductLineRepository.findById(productLineId);
		if (vProductLineData.isPresent()) {
			try {
				Product vProduct = new Product();
				vProduct.setProductCode(paramProduct.getProductCode());
				vProduct.setProductName(paramProduct.getProductName());
				vProduct.setProductDescripttion(paramProduct.getProductDescripttion());
				vProduct.setProductLine(vProductLineData.get());
				vProduct.setProductScale(paramProduct.getProductScale());
				vProduct.setProductVendor(paramProduct.getProductVendor());
				vProduct.setQuantityInStock(paramProduct.getQuantityInStock());
				vProduct.setBuyPrice(paramProduct.getBuyPrice());
				Product vProductSave = gProductRepository.save(vProduct);
				return new ResponseEntity<>(vProductSave, HttpStatus.CREATED);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
						.body("Failed to Create specified Product: " + e.getCause().getCause().getMessage());
			}
		} else {
			ProductLine vProductLineNull = new ProductLine();
			return new ResponseEntity<>(vProductLineNull, HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping("/product/update/{id}/{productLineId}")
	public ResponseEntity<Object> updateProduct(@PathVariable Integer id, @PathVariable Integer productLineId,
			@Valid @RequestBody Product paramProduct) {
		Optional<Product> vProductData = gProductRepository.findById(id);
		if (vProductData.isPresent()) {
			Optional<ProductLine> vProductLineData = gProductLineRepository.findById(productLineId);
			if (vProductLineData.isPresent()) {
				try {
					Product vProduct = vProductData.get();
					vProduct.setProductCode(paramProduct.getProductCode());
					vProduct.setProductName(paramProduct.getProductName());
					vProduct.setProductDescripttion(paramProduct.getProductDescripttion());
					vProduct.setProductLine(vProductLineData.get());
					vProduct.setProductScale(paramProduct.getProductScale());
					vProduct.setProductVendor(paramProduct.getProductVendor());
					vProduct.setQuantityInStock(paramProduct.getQuantityInStock());
					vProduct.setBuyPrice(paramProduct.getBuyPrice());
					Product vProductSave = gProductRepository.save(vProduct);
					return new ResponseEntity<>(vProductSave, HttpStatus.OK);
				} catch (Exception e) {
					return ResponseEntity.unprocessableEntity()
							.body("Failed to Update specified Product: " + e.getCause().getCause().getMessage());
				}
			} else {
				ProductLine vProductLineNull = new ProductLine();
				return new ResponseEntity<>(vProductLineNull, HttpStatus.NOT_FOUND);
			}
		} else {
			Product vProductNull = new Product();
			return new ResponseEntity<>(vProductNull, HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/product/delete/{id}")
	private ResponseEntity<Object> deleteProductById(@PathVariable Integer id) {
		Optional<Product> vProductData = gProductRepository.findById(id);
		if (vProductData.isPresent()) {
			try {
				gProductRepository.deleteById(id);
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			Product vProductNull = new Product();
			return new ResponseEntity<>(vProductNull, HttpStatus.NOT_FOUND);
		}
	}
}
